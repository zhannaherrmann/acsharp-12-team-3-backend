﻿namespace Domain.Shared.Models;

public sealed class Money : IEquatable<Money>
{
    public required Currency Currency { get; init; }
    public required long Amount { get; init; }

    public long Units => Currency.GetUnits(Amount);

    public long MinorUnits => Currency.GetMinorUnits(Amount);

    public Money Add(Money money)
    {
        EnsureCurrenciesAreSame(money);
        return new Money() { Currency = Currency, Amount = Amount + money.Amount };
    }

    public Money Subtract(Money money)
    {
        EnsureCurrenciesAreSame(money);
        return new Money() { Currency = Currency, Amount = Amount - money.Amount };
    }

    public bool Equals(Money? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Currency.Equals(other.Currency) && Amount == other.Amount;
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is Money other && Equals(other);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Currency, Amount);
    }

    private void EnsureCurrenciesAreSame(Money money)
    {
        if (!Currency.Equals(money.Currency))
        {
            throw new InvalidOperationException("Currencies are different");
        }
    }
}
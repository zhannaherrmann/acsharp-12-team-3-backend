﻿namespace Domain.Shared.Models;

public sealed class ExchangeRate
{
    public required long Id { get; set; }
    public required Money Base { get; set; }
    public required Money Transaction { get; set; }
    public required DateTime Date { get; set; }

    public Money Convert(Money input)
    {
        if (!input.Currency.Equals(Base.Currency))
        {
            throw new InvalidOperationException("Input currency must be same as base currency");
        }

        return new Money()
        {
            Currency = Transaction.Currency,
            Amount = input.Amount * Transaction.Amount / Base.Amount
        };
    }
}
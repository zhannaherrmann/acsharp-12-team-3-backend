﻿using Domain.Shared.Enums;

namespace Domain.Shared.Models;

public sealed class Record
{
    public required long Id { get; set; }
    public required string Name { get; set; }
    public required RecordType RecordType { get; set; }
    public required Money MoneyValue { get; set; }
    public required string Source { get; set; }

    /// <summary>
    /// Надо обсудить по смене типа на DateOnly, и добавление хешсета для признаков статьи
    /// </summary>
    public required DateTime CreationDate { get; set; }

    public required long RecordGroupId { get; set; }
}
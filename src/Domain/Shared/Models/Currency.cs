﻿namespace Domain.Shared.Models;

public sealed class Currency : IEquatable<Currency>
{
    public required string Code { get; init; }
    public required int MinorUnits { get; init; }

    public long GetUnits(long amount)
    {
        return amount/MinorUnits;
    }

    public long GetMinorUnits(long amount)
    {
        return amount % MinorUnits;
    }

    public Money ToMoney(long amount)
    {
        return new Money { Currency = this, Amount = amount };
    }

    public Money ToMoney(long units, long minorUnits)
    {
        return ToMoney(units * MinorUnits + minorUnits);
    }

    public bool Equals(Currency? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Code == other.Code && MinorUnits == other.MinorUnits;
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is Currency other && Equals(other);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Code, MinorUnits);
    }
}
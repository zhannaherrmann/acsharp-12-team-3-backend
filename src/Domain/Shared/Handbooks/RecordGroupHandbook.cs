﻿namespace Domain.Shared.Handbooks;

/// <summary>
/// Справочник групп статей
/// </summary>
public class RecordGroupHandbook
{
    public required long Id { get; set; }
    public required string Name { get; set; }
}
﻿namespace Domain.Shared.Handbooks;

/// <summary>
/// Справочник валют
/// </summary>
public class CurrencyHandbook
{
    public required string Name { get; set; }
    public required string Code { get; set; }
}
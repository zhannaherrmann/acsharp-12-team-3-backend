﻿using System.Text.Json.Serialization;

namespace Domain.Shared.Enums;

[JsonConverter(typeof(JsonStringEnumConverter))]
public enum RecordType
{
    Expense = 1,
    Revenue
}
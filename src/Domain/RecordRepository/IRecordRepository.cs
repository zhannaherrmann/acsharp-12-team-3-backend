﻿using Domain.Shared.Models;

namespace Domain.RecordRepository;

public interface IRecordRepository
{
    Task<List<Record>> GetRecordsAsync();
    Task CreateRecordAsync(Record record);
}
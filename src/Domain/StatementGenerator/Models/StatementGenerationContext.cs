using Domain.Shared.Models;

namespace Domain.StatementGenerator.Models;

public class StatementGenerationContext
{
    public required List<Record> Records { get; set; }
    public required string Template { get; set; }
    public required Currency Currency { get; set; }
    public required List<ExchangeRate> ExchangeRates { get; set; }
    public required DateTime PrimaryDate { get; set; }
    public required DateTime SecondaryDate { get; set; }
}
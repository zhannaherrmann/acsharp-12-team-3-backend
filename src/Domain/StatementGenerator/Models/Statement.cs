using Domain.Shared.Models;

namespace Domain.StatementGenerator.Models;

public sealed class Statement
{
    public required long Id { get; set; }
    public required string Name { get; set; }
    public required List<Record> Records { get; set; }
    public required Dictionary<string, string> KeyValues { get; set; }
}
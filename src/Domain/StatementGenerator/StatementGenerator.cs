using Domain.Shared.Enums;
using Domain.Shared.Models;
using Domain.StatementGenerator.Interfaces;
using Domain.StatementGenerator.Models;

namespace Domain.StatementGenerator;

public class StatementGenerator : IStatementGenerator
{
    public Statement Generate(StatementGenerationContext context)
    {
        var statement = new Statement
        {
            Id = 1,
            Name = " ",
            Records = new List<Record>(),
            KeyValues = new Dictionary<string, string>()
        };

        var totalRevenue = new Money
        {
            Currency = context.Currency,
            Amount = 0
        };

        var totalExpense = new Money
        {
            Currency = context.Currency,
            Amount = 0
        };


        if (context.Template == "IncomeStatement")
        {
            statement.Name = "IncomeStatement";
            foreach (var contextRecord in context.Records)
            {
                if (contextRecord.RecordType == RecordType.Revenue)
                {
                    totalRevenue = totalRevenue.Add(contextRecord.MoneyValue);
                    statement.Records.Add(contextRecord);
                }
            }

            // Поле Total отсутствует в модели Statement, поэтому создается total в словаре со значением.
            statement.KeyValues.Add("Total", totalRevenue.Amount.ToString());
        }
        else if (context.Template == "LossStatement")
        {
            statement.Name = "LossStatement";
            foreach (var contextRecord in context.Records)
            {
                if (contextRecord.RecordType == RecordType.Expense)
                {
                    totalExpense = contextRecord.MoneyValue.Add(contextRecord.MoneyValue);
                    statement.Records.Add(contextRecord);
                }
            }

            // Поле Total отсутствует в модели Statement, поэтому создается total в словаре со значением.
            statement.KeyValues.Add("Total", totalExpense.Amount.ToString());
        }

        return statement;
    }
}
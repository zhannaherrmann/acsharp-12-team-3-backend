using Domain.StatementGenerator.Models;

namespace Domain.StatementGenerator.Interfaces;

public interface IStatementGenerator
{
    Statement Generate(StatementGenerationContext context);
}
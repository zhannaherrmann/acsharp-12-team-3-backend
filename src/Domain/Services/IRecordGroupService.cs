﻿using Domain.Shared.Handbooks;
using Domain.Shared.Models;

namespace Domain.Services;

public interface IRecordGroupService
{
    Task<List<RecordGroupHandbook>> GetAsync();
    Task CreateAsync(RecordGroupHandbook recordGroup);
}
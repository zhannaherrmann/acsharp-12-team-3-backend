﻿using Domain.Shared.Handbooks;

namespace Domain.Services;

public interface ICurrencyService
{
    Task<List<CurrencyHandbook>> GetAsync();
    Task CreateAsync(CurrencyHandbook currency);
}
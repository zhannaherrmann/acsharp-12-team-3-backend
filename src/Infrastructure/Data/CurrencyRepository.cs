﻿using System.Text.Json;
using Domain.Services;
using Domain.Shared.Handbooks;

namespace Infrastructure.Data;

public class CurrencyRepository : ICurrencyService
{
    public async Task<List<CurrencyHandbook>> GetAsync()
    {
        string file = await File.ReadAllTextAsync("./CurrencyData.json");
        List<CurrencyHandbook> currrencies = JsonSerializer.Deserialize<List<CurrencyHandbook>>(file);
        return currrencies;
    }

    public async Task CreateAsync(CurrencyHandbook currency)
    {
        string file = await File.ReadAllTextAsync("./CurrencyData.json");
        List<CurrencyHandbook>? currencies = JsonSerializer.Deserialize<List<CurrencyHandbook>>(file);
        currencies?.Add(currency);

        var jsonOptions = new JsonSerializerOptions
        {
            WriteIndented = true,
        };

        var jsonData = JsonSerializer.Serialize(currencies, jsonOptions);
        File.WriteAllText("../../../CurrencyData.json", jsonData);
    }
}
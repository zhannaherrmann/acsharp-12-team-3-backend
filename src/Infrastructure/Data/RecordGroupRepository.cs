﻿using System.Text.Json;
using Domain.Services;
using Domain.Shared.Handbooks;
using Domain.Shared.Models;

namespace Infrastructure.Data;

public class RecordGroupRepository : IRecordGroupService
{
    public async Task<List<RecordGroupHandbook>> GetAsync()
    {
        string file = await File.ReadAllTextAsync("./RecordGroupData.json");
        List<RecordGroupHandbook> recordGroups = JsonSerializer.Deserialize<List<RecordGroupHandbook>>(file);
        return recordGroups;
    }

    public async Task CreateAsync(RecordGroupHandbook recordGroup)
    {
        string file = await File.ReadAllTextAsync("./RecordGroupData.json");
        List<RecordGroupHandbook> recordGroups = JsonSerializer.Deserialize<List<RecordGroupHandbook>>(file);
        recordGroups?.Add(recordGroup);

        var jsonOptions = new JsonSerializerOptions
        {
            WriteIndented = true,
        };

        var jsonData = JsonSerializer.Serialize(recordGroups, jsonOptions);
        await File.WriteAllTextAsync("../../../RecordGroupData.json", jsonData);
    }
}
﻿using System.Text.Json;
using Domain.RecordRepository;
using Domain.Services;
using Domain.Shared.Models;

namespace Infrastructure;

public class RecordRepository : IRecordRepository
{
    public async Task<List<Record>> GetRecordsAsync()
    {
        var file = await File.ReadAllTextAsync("./data.json");
        List<Record> records = JsonSerializer.Deserialize<List<Record>>(file);
        return records;
    }

    public async Task CreateRecordAsync(Record record)
    {
        var file = await File.ReadAllTextAsync("./data.json");
        List<Record> records = JsonSerializer.Deserialize<List<Record>>(file);
        records?.Add(record);

        var jsonOptions = new JsonSerializerOptions
        {
            WriteIndented = true,
        };

        var jsonData = JsonSerializer.Serialize(records, jsonOptions);
        await File.WriteAllTextAsync("./data.json", jsonData);
    }
}
﻿namespace WebApi.Dto;

public class StatementRequestDto
{
    public required string Type { get; set; }
    public required string Currency { get; set; }
    public required string Period { get; set; }
}
﻿using Domain.RecordRepository;
using Domain.Services;
using Domain.Shared.Models;
using Domain.StatementGenerator;
using Domain.StatementGenerator.Models;

namespace WebApi.Services;

public class RecordService
{
    private readonly IRecordRepository _recordRepository;

    public RecordService(IRecordRepository recordRepository)
    {
        _recordRepository = recordRepository;
    }
    
    public Statement GetStatement(string type, string currency, string period)
    {
        var context = new StatementGenerationContext
        {
            Records = GetRecordsByPeriod(period),
            Template = type.ToLower() == "expense" ? "LossStatement" : "IncomeStatement",
            Currency = new Currency
            {
                Code = currency,
                MinorUnits = 100
            },
            ExchangeRates = new List<ExchangeRate>(),
            PrimaryDate = default,
            SecondaryDate = default
        };

        var generator = new StatementGenerator();
        var statement = generator.Generate(context);
        var kztRate = new ExchangeRate
        {
            Id = 0,
            Base = new Currency
            {
                Code = "USD",
                MinorUnits = 100
            }.ToMoney(1, 0),
            Transaction = new Currency
            {
                Code = "KZT",
                MinorUnits = 100
            }.ToMoney(450, 0),
            Date = DateTime.Today
        };

        foreach (var record in statement.Records
                     .Where(record => record.MoneyValue.Currency.Code != "KZT"))
        {
            record.MoneyValue = kztRate.Convert(record.MoneyValue);
        }
        
        return statement;
    }
    
    private List<Record> GetRecordsByPeriod(string period)
    {
        var records = GetRecords().Result;
        var periodRecords = new List<Record>();
        foreach (Record record in records)
        {
            if (record.CreationDate.Month == GetMonth(period))
            {
                periodRecords.Add(record);
            }
        }

        return periodRecords;
    }
    
    private async Task<List<Record>> GetRecords()
    {
        var recordsList = await _recordRepository.GetRecordsAsync();
        return recordsList;
    }
    
    private int GetMonth(string period)
    {
        switch (period)
        {
            case "01" :
                return 1;
            case "02" :
                return 2;
            case "03" :
                return 3;
            case "04" :
                return 4;
            case "05" :
                return 5;
            case "06" :
                return 6;
            case "07" :
                return 7;
            case "08" :
                return 8;
            case "09" :
                return 9;
            case "10" :
                return 10;
            case "11" :
                return 11;
            case "12" :
                return 12;
            default:
                return 11;
        }
    }
}
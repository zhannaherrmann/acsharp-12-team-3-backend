﻿using Domain.RecordRepository;
using Domain.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[ApiController]
[Route("api/[controller]")]
public class RecordsController : Controller
{
    private readonly IRecordRepository _recordRepository;

    public RecordsController(IRecordRepository recordRepository)
    {
        _recordRepository = recordRepository;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var records = await _recordRepository.GetRecordsAsync();
        return Ok(records);
    }

    [HttpPost]
    public async Task<IActionResult> Post(Record vm)
    {
        if (vm is not null)
        {
            var record = new Record()
            {
                Id = vm.Id, Name = vm.Name, RecordType = vm.RecordType, MoneyValue = vm.MoneyValue,
                Source = vm.Source, CreationDate = vm.CreationDate, RecordGroupId = vm.RecordGroupId
            };

            await _recordRepository.CreateRecordAsync(record);
        }
        else
        {
            return BadRequest();
        }

        return Ok(vm);
    }
}
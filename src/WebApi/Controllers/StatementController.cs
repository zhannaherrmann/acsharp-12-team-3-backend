﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Dto;
using WebApi.Services;


namespace WebApi.Controllers;

/// <summary>
/// Возвращает Statement ввиде json на фронт
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class StatementController : ControllerBase
{
    private readonly RecordService _recordService;

    public StatementController(RecordService recordService)
    {
        _recordService = recordService;
    }

    [HttpPost("post")]
    public IActionResult GetStatement([FromBody] StatementRequestDto model)
    {
        if (string.IsNullOrEmpty(model.Type))
        {
            return NotFound();
        }

        var statement = _recordService.GetStatement(model.Type, model.Currency, model.Period);

        return Ok(statement);
    }
}
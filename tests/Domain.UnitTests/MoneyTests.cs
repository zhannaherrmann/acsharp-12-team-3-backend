using Domain.UnitTests.Fixtures;

namespace Domain.UnitTests;

public class MoneyTests
{
    [Theory]
    [InlineData(200, 0, 200)]
    [InlineData(0, 0, 0)]
    [InlineData(300, 200, 500)]
    public void Should_Add_Currencies(long left, long right, long expected)
    {
        //Arrange
        var leftMoney = CurrencyFixtures.Kzt.ToMoney(left);
        var rightMoney = CurrencyFixtures.Kzt.ToMoney(right);

        //Act 
        var result = leftMoney.Add(rightMoney);

        //Assert
        result.Should().Be(CurrencyFixtures.Kzt.ToMoney(expected));
    }

    [Theory]
    [InlineData(100, 100, 0)]
    [InlineData(200, 100, 100)]
    public void Should_Subtract_Currencies(long left, long right, long expected)
    {
        //Arrange
        var leftMoney = CurrencyFixtures.Kzt.ToMoney(left);
        var rightMoney = CurrencyFixtures.Kzt.ToMoney(right);

        //Act 
        var result = leftMoney.Subtract(rightMoney);

        //Assert
        result.Should().Be(CurrencyFixtures.Kzt.ToMoney(expected));
    }

    [Fact]
    public void Should_Throw_When_Different_Currencies_Are_Added()
    {
        // Arrange
        var leftMoney = CurrencyFixtures.Usd.ToMoney(100);
        var rightMoney = CurrencyFixtures.Kzt.ToMoney(100);

        // Act
        var act = void () => leftMoney.Add(rightMoney);

        // Assert
        act.Should().Throw<InvalidOperationException>();
    }

    [Fact]
    public void Should_Throw_When_Different_Currencies_Are_Subtracted()
    {
        // Arrange
        var leftMoney = CurrencyFixtures.Usd.ToMoney(100);
        var rightMoney = CurrencyFixtures.Kzt.ToMoney(100);

        // Act
        var act = void () => leftMoney.Subtract(rightMoney);

        // Assert
        act.Should().Throw<InvalidOperationException>();
    }
}
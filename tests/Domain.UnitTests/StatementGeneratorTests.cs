using Domain.UnitTests.Fixtures;

namespace Domain.UnitTests;

public class StatementGeneratorTests
{
    [Fact]
    public void Should_Generate_Statement()
    {
        // Arrange
        var context = ContextFixtures.CreateContext();
        var generator = new StatementGenerator.StatementGenerator();

        // Act
        var statement = generator.Generate(context);

        // Arrange
        statement.KeyValues.Should().Contain("Total", "325000");
    }
    
    [Fact]
    public void Should_ContextRecord_Name_LossStatement()
    {
        // Arrange
        var context = ContextFixtures.CreateExpenseContext();
        var generator = new StatementGenerator.StatementGenerator();

        // Act
        var statement = generator.Generate(context);

        // Arrange
        statement.Name.Should().Contain("LossStatement");
    }
}
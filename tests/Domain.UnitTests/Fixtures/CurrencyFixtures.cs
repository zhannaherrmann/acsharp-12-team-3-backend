using Domain.Shared.Models;

namespace Domain.UnitTests.Fixtures;

public class CurrencyFixtures
{
    public static readonly Currency Kzt = new Currency(){Code = "kzt", MinorUnits = 100};
    public static readonly Currency Usd = new Currency { Code = "usd", MinorUnits = 100 };
}
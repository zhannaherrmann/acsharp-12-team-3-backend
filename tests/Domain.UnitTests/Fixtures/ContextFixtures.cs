using Domain.Services;
using Domain.Shared.Enums;
using Domain.Shared.Models;
using Domain.StatementGenerator.Models;
using Record = Domain.Shared.Models.Record;

namespace Domain.UnitTests.Fixtures;

public static class ContextFixtures
{
    public static StatementGenerationContext CreateContext()
    {
        return new()
        {
            Records = new List<Record>
            {
                new Record
                {
                    Id = 1,
                    Name = "Bank",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(1000,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 2,
                    Name = "Grant",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(2000,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 3,
                    Name = "Students",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(250,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 4,
                    Name = "Internet",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(250,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 5,
                    Name = "Materials",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(1000,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 6,
                    Name = "CommissionOfTheBank",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(500,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 7,
                    Name = "Telephone",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(0,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
            },
            Template = "IncomeStatement",
            Currency = CurrencyFixtures.Kzt,
            ExchangeRates = new List<ExchangeRate>(),
            PrimaryDate = DateTime.Now,
            SecondaryDate = DateTime.Now
        };
    }
    
    public static StatementGenerationContext CreateExpenseContext()
    {
        return new()
        {
            Records = new List<Record>
            {
                new Record
                {
                    Id = 1,
                    Name = "Bank",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(1000,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 2,
                    Name = "Grant",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(2000,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 3,
                    Name = "Students",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(250,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Revenue,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 4,
                    Name = "Internet",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(250,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 5,
                    Name = "Materials",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(1000,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 6,
                    Name = "CommissionOfTheBank",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(500,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
                new Record
                {
                    Id = 7,
                    Name = "Telephone",
                    MoneyValue = CurrencyFixtures.Kzt.ToMoney(0,
                        0),
                    Source = "Telegram",
                    CreationDate = DateTime.Now,
                    RecordType = RecordType.Expense,
                    RecordGroupId = 1
                },
            },
            Template = "LossStatement",
            Currency = CurrencyFixtures.Kzt,
            ExchangeRates = new List<ExchangeRate>(),
            PrimaryDate = DateTime.Now,
            SecondaryDate = DateTime.Now
        };
    }
}
using Domain.Shared.Enums;
using Domain.Shared.Models;
using Record = Domain.Shared.Models.Record;

namespace Domain.UnitTests.Fixtures;

public static class RecordFixtures
{
    public static Record Expense(Money amount) =>
        new Record
        {
            Id = 1,
            MoneyValue = amount,
            RecordType = RecordType.Expense,
            Source = "Anything",
            CreationDate = DateTime.Now,
            RecordGroupId = 1,
            Name = null,
        };

    public static Record Revenue(Money amount)
    {
        return new Record
        {
            Id = 1,
            MoneyValue = amount,
            RecordType = RecordType.Revenue,
            Source = "Anything",
            CreationDate = DateTime.Now,
            RecordGroupId = 1,
            Name = null,
        };
    }
}
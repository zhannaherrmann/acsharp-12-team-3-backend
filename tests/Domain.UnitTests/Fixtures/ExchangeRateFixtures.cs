using Domain.Shared.Models;

namespace Domain.UnitTests.Fixtures;

public static class ExchangeRateFixtures
{
    public static readonly ExchangeRate KztToUsd = new()
    {
        Id = 1,
        Base = CurrencyFixtures.Kzt.ToMoney(500, 0),
        Transaction = CurrencyFixtures.Usd.ToMoney(1, 0),
        Date = DateTime.MinValue
    };

    public static readonly ExchangeRate UsdToKzt = new()
    {
        Id = 2,
        Base = CurrencyFixtures.Usd.ToMoney(1, 0),
        Transaction = CurrencyFixtures.Kzt.ToMoney(500, 0),
        Date = DateTime.MinValue 
    };
}
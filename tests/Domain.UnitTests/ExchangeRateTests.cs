using Domain.UnitTests.Fixtures;

namespace Domain.UnitTests;

public class ExchangeRateTests
{
    [Theory]
    [InlineData(500, 0, 1, 0)]
    [InlineData(250, 0, 0, 50)]
    [InlineData(5, 0, 0, 1)]
    [InlineData(4, 0, 0, 0)]
    public void Should_Convert_To_More_Expensive_Currency(
        long inputUnits,
        long inputMinorUnits,
        long expectedUnits,
        long expectedMinorUnits)
    {
        //Arrange
        var input = CurrencyFixtures.Kzt.ToMoney(inputUnits, inputMinorUnits);

        //Act 
        var result = ExchangeRateFixtures.KztToUsd.Convert(input);

        //Assert
        result.Should().Be(CurrencyFixtures.Usd.ToMoney(expectedUnits, expectedMinorUnits));
    }

    [Fact]
    public void Should_Convert_Be_Currency()
    {
        //Arrange
        var exchangeRate = ExchangeRateFixtures.KztToUsd;

        //Act 
        var act = () => exchangeRate.Convert(CurrencyFixtures.Usd.ToMoney(200));

        //Assert
        act.Should().Throw<InvalidOperationException>();
    }
}
﻿using Domain.Shared.Models;

namespace Domain.UnitTests;

public class CurrencyTests
{
    [Theory]
    [InlineData(500, 5)]
    [InlineData(499, 4)]
    public void Should_Calculate_Units(long input, long expected)
    {
        //Arrange
        var kzt = new Currency
        {
            Code = "KZT",
            MinorUnits = 100
        };
        
        //Act 

        var result = kzt.GetUnits(input);
        
        //Assert
        result.Should().Be(expected);
    }

    [Theory]
    [InlineData(500, 0)]
    [InlineData(499, 99)]
    public void Should_Calculate_Minor_Units(long input, long expected)
    {
        //Arrange
        var kzt = new Currency
        {
            Code = "KZT",
            MinorUnits = 100
        };
        
        //Act 

        var result = kzt.GetMinorUnits(input);
        
        //Assert
        result.Should().Be(expected);
    }
}